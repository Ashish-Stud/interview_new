package com.agarwal.ashish.interview.injection.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import com.agarwal.ashish.interview.data.DataManager;
import com.agarwal.ashish.interview.data.remote.ApiService;
import com.agarwal.ashish.interview.injection.ApplicationContext;
import com.agarwal.ashish.interview.injection.module.ApplicationModule;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    @ApplicationContext Context context();
    Application application();
    ApiService apiService();
    DataManager dataManager();

}
