package com.agarwal.ashish.interview.injection.component;

import dagger.Subcomponent;
import com.agarwal.ashish.interview.injection.PerActivity;
import com.agarwal.ashish.interview.injection.module.ActivityModule;
import com.agarwal.ashish.interview.ui.main.MainActivity;

/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

}
