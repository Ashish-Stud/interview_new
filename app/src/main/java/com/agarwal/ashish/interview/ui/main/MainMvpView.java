package com.agarwal.ashish.interview.ui.main;

import java.util.List;

import com.agarwal.ashish.interview.data.remote.model.SearchData;
import com.agarwal.ashish.interview.ui.base.MvpView;

public interface MainMvpView extends MvpView {

    void setErrorState();

    void setList(List<SearchData> searchData);
}
