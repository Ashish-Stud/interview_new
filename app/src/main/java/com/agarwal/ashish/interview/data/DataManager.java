package com.agarwal.ashish.interview.data;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

import com.agarwal.ashish.interview.data.remote.ApiService;

@Singleton
public class DataManager {

    private final ApiService mApiService;

    @Inject
    public DataManager(ApiService apiService) {
        mApiService = apiService;
    }

    public ApiService getmApiService() {
        return mApiService;
    }
}
